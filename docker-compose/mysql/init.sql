DROP TABLE IF EXISTS `php-contact-form`.email;

CREATE TABLE `php-contact-form`.email (
	`id` BIGINT auto_increment NOT NULL,
	`to` varchar(100) NOT NULL,
	`from` varchar(100) NOT NULL,
	`subject` varchar(255) NOT NULL,
	`plain_text_email` TEXT NOT NULL,
	`html_email` TEXT NULL,
	`contact_id` BIGINT NOT NULL,
	`date_created` DATETIME NOT NULL,
	`date_modified` TIMESTAMP NOT NULL,
	CONSTRAINT email_PK PRIMARY KEY (id),
	CONSTRAINT email_FK FOREIGN KEY (id) REFERENCES `php-contact-form`.contact(id)
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

DROP TABLE IF EXISTS `php-contact-form`.contact;

CREATE TABLE `php-contact-form`.contact (
	`id` BIGINT auto_increment NOT NULL,
	`full_name` varchar(100) NOT NULL,
	`email_address` varchar(100) NOT NULL,
	`phone` varchar(25),
	`date_created` DATETIME NOT NULL,
	`date_modified` TIMESTAMP NOT NULL,
	CONSTRAINT contact_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;