$(function() {
    // process the form
    $('#contact #contact-form').submit(function(e) {
        console.log('submit');
        var formData = {
            'name' : $('input[name=ful-name]').val(),
            'email' : $('input[name=email]').val(),
            'phone' : $('input[name=phone]').val(),
            'message' : $('textarea[name=message]').val()
        };
        console.log(formData);
        $.ajax({
            type : 'POST',
            url : '/post-ajax',
            data : formData,
            dataType : 'json'
        })
        .done(function(data) {
            if (data.isError) {
                $('#error-section').removeClass('error success').addClass('error');
                $('#error-section .message').html(data.message);
            } else {
                $('#error-section').removeClass('error success').addClass('success');
                $('#error-section .message').html(data.message);
            }
            setInterval(errorTimer, 3000);
        })
        .fail(function(data){
            $('#error-section').removeClass('error success').addClass('error');
            $('#error-section .message').html('Error Posting');
            setInterval(errorTimer, 6000);
        });
        e.preventDefault();
    });
});

function errorTimer() {
    $('#error-section').fadeOut( "slow", function() {
        removeClass('error success');
        $('#error-section .message').html('');
    })
}