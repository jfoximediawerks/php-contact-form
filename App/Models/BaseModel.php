<?php

namespace App\Models;

use Core\Model;

class BaseModel extends Model 
{
    protected $id;
    protected $date_created;
    protected $date_modified;

    public function __construct()
    {

    }
    public function save()
    {
        if (isset($this->id)) {
            return $this->update();
        }

        return $this->insert();
    }

    private function insert()
    {
        $this->setDateCreated(date('Y-m-d H:i:s'));
        try{
            $db = $this->getDB();
            $query = $this->generateInsertQuery();
            $db->prepare($query);
            foreach ($this as $key => $value) {
                $db->bindParam($key, $value);
            }
            $db->execute();
            $this->setId($db->lastInsertId());
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function generateInsertQuery()
    {
        $table = get_class($this);
        $query = "INSERT INTO `{$table}` ";
        $count = 0;
        $keys = '';
        $values = '';
        foreach ($this as $key => $value ) {
            if ($count == 0) {
                $keys = "(`$key`";
                $values = "(:$key";
            }
            $keys .= ", `$key`";
            $values .= ", :$key"; 

            $count++;
        }
        $query.= $keys.') Values '.$values.')';

        return $query;
    }

    private function update()
    {
        try {
            $db = Model::getDB();
            $query = $this->generateUpdateQuery();
            $db->prepare($query);
            foreach ($this as $key => $value) {
                $db->bindParam($key, $value);
            }
            $db->execute();
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }

    private function generateUpdateQuery()
    {
        $table = get_class($this);
        $query = "UPDATE `$table` SET ";
        $count = 0;
        foreach ($this as $key => $value ) {
            if ($count == 0) {
                $query .= '`'.$key.'` = :'.$key.'';
            }
            $query .= ', `'.$key.'` = :'.$key.'';
            $count++;
        }
        $query = 'WHERE id = '.$this->id;

        return $query;
    }
    private function setId(int $id)
    {
        $this->id = $id;
    }

    public function getId():int
    {
        return $this->id;
    }

    public function getDateCreated()
    {
        return $this->date_created;
    }

    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    public function getDateModified()
    {
        return $this->date_modified;
    }

    public function setDateModified($date_modified)
    {
        $this->date_modified = $date_modified;
    }
}