<?php

namespace App\Models;

use App\Models\BaseModel;

class Contact extends BaseModel
{

    private $full_name;
    private $email_address;
    private $message;
    private $phone;

    public function __construct(string $name, string $email, string $message, string $phone = null)
    {
        parent::__construct();
        $this->full_name = $name;
        $this->email_address = $email;
        $this->$message = $message;
        $this->phone = $phone;
    }

    public function getFullName():string
    {
        return $this->full_name;
    }

    public function setFullName($name)
    {
        $this->full_name = $name;
    }

    public function getEmailAddress():string
    {
        return $this->emailAddress;
    }

    public function setEmail($email)
    {
        $this->emailAddress = $email;
    }

    public function getMessage():string
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getphone():string
    {
        return $this->phone;
    }

    public function setphone($phone)
    {
        $this->phone = $phone;
    }

}