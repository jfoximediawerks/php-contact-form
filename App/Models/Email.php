<?php

namespace App\Models;

use App\Models\BaseModel;

class Email extends BaseModel
{

    private $to;
    private $from;
    private $subject;
    private $plain_text_email;
    private $contact_id;
    private $html_email;


    public function __construct(string $to, string $from, string $subject, string $plain_text_email, int $contact_id, string $html_email = null)
    {
        parent::__construct();
        $this->setTo($to);
        $this->setFrom($from);
        $this->setSubject($subject);
        $this->setPlainTextEmail($plain_text_email);
        $this->setContactId($contact_id)
        $this->setHtmlEmail($html_email);
    }

    public function getTo():string
    {
        return $this->to;
    }

    public function setTo($to)
    {
        $this->to = $to;
    }


    public function getFrom():string
    {
        return $this->to;
    }

    public function setFrom($from)
    {
        $this->from = $from;
    }

    public function getSubject():string
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function getPlainTextEmail():string
    {
        return $this->plain_text_email;
    }

    public function setPlainTextEmail($plain_text_email)
    {
        $this->plain_text_email = $plain_text_email;
    }

    public function getHtmlEmail():string?
    {
        return $this->html_email;
    }

    public function setHtmlEmail($html_email)
    {
        $this->html_email = $html_email;
    }

    public function getContactId()
    {
        return $this->contact_id;
    }

    public function setContactId($contact_id)
    {
        $this->contact_id = $contact_id;
    }
}