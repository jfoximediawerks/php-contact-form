<?php 

require './Router.php';
use App as App;

$router = new App\Router();

/**
 * Index Routes
 */

$router::add('', ['controller' => 'Index', 'action'=>'index']);
$router::add('index/post', ['controller' => 'Index', 'action'=>'post']);