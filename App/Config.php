<?php

namespace App;

/**
 * Application configuration
 *
 * PHP version 5.4
 */
class Config
{

    /**
     * Database host
     * @var string
     */
    const DB_HOST = '127.0.0.1';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'php-contact-form';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'phpuser';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = 'phppassword';

    /**
     * Database port
     * @var string
     */
    const DB_PORT = '3306';

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = true;

    /**
     * SMTP Configuration
     * 
     */
    const SMTP_HOST = '';
    const SMTP_SMTP_AUTH = true;
    const SMTP_USERNAME = '';
    const SMTP_PASSWORD = '';
    const SMTP_PORT = 587;
    const DEFAULT_EMAIL = '';
}
