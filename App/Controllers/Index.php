<?php

namespace App\Controllers;

use Core\View;
use App\Config;
use App\Models\Email;
use App\Models\Contact;
use App\Services\EmailServiService;
use App\Services\ValidationService;

class Index {

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        if(!empty($_POST)){
            $this->postContact($_POST);
        } 
        View::renderTemplate('index.php');
    }

    public function postAction()
    {
        //echo "test";
        //print_r($_POST);
        $message = $this->postContact($_POST);
        View::renderTemplate('index.php', $message);
    }

    public function postAjaxAction()
    {
        $result = $this->postContact($_POST);
        return json_encode($result);
    }

    private function postContact(Array $post):Array
    {
        if (!isset($post['full-name'])) {
            return ['isError'=>true, 'message'=>'Full Name Required'];
        }
        if (!isset($post['email'])) {
            return ['isError'=>true, 'message'=>'Email Required'];
        }
        if (!isset($post['message'])) {
            return ['isError'=>true, 'message'=>'message Required'];
        }

        if (isset($post['phone'])) {
            $phone = $post['phone'];
            $contact = new Contact($post['full-name'], $post['email'], $post['message'], $post['phone']);
        } else {
            $contact = new Contact($post['full-name'], $post['email'], $post['message']);
        } 
        
        $contact->save();
        $email = new Email($post['email'], Config::DEFAULT_EMAIL, "{$post['full-name']} Your Message Was Received", $post['message'], $contact->getId());

        $email_service = new ContactEmailerService($email,$contact);
        $email_service->sendContactEmail();
        return ['isError'=>false, 'message' => 'Success'];
    }
}