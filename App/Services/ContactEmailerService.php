<?php 

namespace App\Services;

use PHPMailer\PHPMailer;
use App\Config;
use App\Models\Email;
use App\Models\Contact;

class ContactEmailerService {

    private $email_info;
    private $contact;

    public function __construct(Email $email, Contact $contact)
    {
        $this->email_info = $email;
        $this->contact = $contact;
    }
    public function sendContactEmail()
    {
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->isSMTP();                                            
            $mail->Host = Config::SMTP_HOST;
            $mail->SMTPAuth = true;
            $mail->Username = Config::SMTP_USERNAME;
            $mail->Password = Config::SMTP_PASSWORD;
            $mail->Port = Config::SMTP_PORT;

            //Recipients
            $mail->setFrom($this->email->getFrom());
            $mail->addAddress($this->email->getTo(), $this->contact->getFullName());

            // Content
            //$mail->isHTML(true);
            $mail->Subject = $this->email->getSubject();
            $mail->Body = $this->email->getMessage();
            $mail->AltBody = $this->email->getMessage();

            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
    
}